import Link from "next/link";
import Head from "next/head";
import {Text} from "@geist-ui/react";
import { GetStaticProps, GetStaticPaths } from 'next'
import Layout from "../../components/layout";
import { getAudioDetail, getAudioDetails } from "../../lib/audio";
import {SITE_LOGO, SITE_TITLE} from "../../lib/constants";

export default function Post({ postData }) {
  return (
      <Layout>
        <Head>
          <title>{postData.title} | {SITE_TITLE}</title>

          {postData.text && <meta name="description" content={postData.text.substr(0, 150)} />}
          {postData.tags && postData.tags.length > 0 &&
            <meta name="keywords" content={postData.tags.join(",")} />}

          <script type="application/ld+json">{JSON.stringify({
            "@context": "http://schema.org/",
            "@type": "AudioObject",
            contentUrl: `//audio/${postData.audio}`,
            description: postData.text,
            text: postData.text,
            encodingFormat: "audio/mpeg",
            name: postData.audio,
            accessMode: "auditory",
            author: postData.characters && postData.characters.length > 0 && {
              "@type": "Person",
              name: postData.characters[0]
            },
            keywords: postData.tags && postData.tags.join(","),
          })}</script>
        </Head>

        <Text h1>{SITE_LOGO} {SITE_TITLE}</Text>
        <Text h2>{postData.title}</Text>

        {postData.characters?.map((c, i) => (
          <Text key={i}>{c}</Text>
        ))}


        <h3>
          <Link href="/">
            <a>Back to home</a>
          </Link>
        </h3>
      </Layout>
  )
}


export const getStaticPaths: GetStaticPaths = async () => {
  // Return a list of possible value for id
  const audio = await getAudioDetails();
  return {
    paths: audio.map(a => ({
      params: {
        id: a.id,
      }
    })),
    fallback: false,
  };
}

export const getStaticProps: GetStaticProps = async context => {
  // Fetch necessary data for the blog post using params.id
  return {
    props: {
      postData: await getAudioDetail(context.params.id)
    }
  }
}
