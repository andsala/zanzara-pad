import Document, {Html, Head, Main, NextScript} from 'next/document'
import {CssBaseline} from '@geist-ui/react'
import {SITE_TITLE} from "../lib/constants";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    const styles = CssBaseline.flush()

    return {
      ...initialProps,
      styles: (
        <>
          {initialProps.styles}
          {styles}
        </>
      ),
    }
  }

  render() {
    return (
      <Html>
        <Head>
          <link rel="icon"
                href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>‍🦟</text></svg>"/>
          <link rel="icon" href="/favicon.svg"/>
          <meta name="theme-color" content="white"/>
          <meta name="color-scheme" content="light-only" />
          <meta name="application-name" content={SITE_TITLE} />
        </Head>
        <body>
        <Main/>
        <NextScript/>
        </body>
      </Html>
    )
  }
}

export default MyDocument;
