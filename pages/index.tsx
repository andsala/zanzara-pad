import React, {useEffect, useState} from "react";
import {GetStaticProps} from "next";
import Head from "next/head"
import {useRouter} from "next/router";
import {Grid, Input, Modal, Page, Spacer, Tag, Text, useInput, useModal} from "@geist-ui/react";
import Fuse from "fuse.js"; // TODO check import only type
import AudioPlayer from "../components/AudioPlayer";
import {getAudioDetails} from "../lib/audio";
import {SITE_LOGO, SITE_TITLE} from "../lib/constants";
import {search} from "../lib/search";
import {IAudioItem} from "../lib/model";

export default function Home({audio}) {
  const router = useRouter();
  const {state: inputValue, setState: setInputValue, bindings: searchBindings} = useInput('');
  const [results, setResults] = useState<Fuse.FuseResult<IAudioItem>[]>([]);
  const [selectedItem, setSelectedItem] = useState<IAudioItem>(null);
  const {setVisible, bindings: modalBindings} = useModal()

  // Initialize search value
  useEffect(() => {
    if (!inputValue && router.query.q) {
      setInputValue(router.query.q as string);
    }
  }, [router.query])

  // Perform search on search type
  useEffect(() => {
    router.push({pathname: router.pathname, query: {q: inputValue}})
      .catch(console.warn);

    search(audio, inputValue)
      .then(setResults)
      .catch(console.warn)
  }, [inputValue])

  useEffect(() => setVisible(!!selectedItem), [selectedItem]);

  return (
    <Page>
      <Head>
        <title>{SITE_TITLE}</title>
      </Head>
      <main>
        <Grid.Container justify="space-between" alignItems="center">
          <Grid>
            <Text h1>{SITE_LOGO} {SITE_TITLE}</Text>
          </Grid>
          <Grid xs={24} md={6}>
            <Input placeholder="Search..." width="100%" {...searchBindings}/>
          </Grid>
        </Grid.Container>
        <Spacer y={1.5}/>

        <Grid.Container gap={2} justify="center" alignItems="stretch">
          {results.map(({item}) => (
            <Grid xs={24} sm={12} md={8} lg={6} xl={4} key={item.id}>
              <AudioPlayer item={item} onClick={() => setSelectedItem(item)}/>
            </Grid>
          ))}
        </Grid.Container>

        <Modal {...modalBindings}>
          <Modal.Title>{selectedItem?.title}</Modal.Title>
          {selectedItem?.characters?.length > 0 && <Modal.Subtitle>{selectedItem.characters[0]}</Modal.Subtitle>}
          <Modal.Content>
            <p>{selectedItem?.text}</p>

            <Grid.Container gap={.2}>
              {selectedItem?.characters && selectedItem.characters.map((c, i) => (
                <Grid key={i}>
                  <Tag invert>{c}</Tag>
                </Grid>
              ))}
            </Grid.Container>
          </Modal.Content>
        </Modal>
      </main>
    </Page>
  )
}

export const getStaticProps: GetStaticProps = async context => {
  const data = await getAudioDetails();

  return {
    props: {
      audio: data
    }
  };
}
