import React, {useEffect, useState} from "react";
import {Button, Card, Text, useToasts} from "@geist-ui/react";
import {Download, Pause, Play, Share2} from '@geist-ui/react-icons'
import {IAudioItem} from "../lib/model";

import styles from "./AudioPlayer.module.css";

export function canShare(): boolean {
  return !!navigator?.share;
}

export async function share(item: IAudioItem): Promise<void> {
  const req = await fetch(`/audio/${item.audio}`);
  const audioBlob = await req.blob();

  const files = [new File([audioBlob], item.audio, {type: "audio/mp3"})];

  // @ts-ignore
  if (navigator?.canShare({files: files})) {
    await navigator?.share({
      text: item.title,
      // @ts-ignore
      files: files,
    });
    console.log('ok share');
  } else {
    throw new Error(`Your system doesn't support sharing files.`);
  }
}

export interface IAudioPlayerProps {
  item?: IAudioItem;
  onClick?: () => any;
}

export default function AudioPlayer({item, onClick}: IAudioPlayerProps) {
  const [progressPercent, setProgressPercent] = useState(0);
  const [audioPlayer, setAudioPlayer] = useState<HTMLAudioElement>(null);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);
  const [waiting, setWaiting] = useState(false);
  const [toasts, setToast] = useToasts()

  function stop() {
    if (audioPlayer) {
      audioPlayer.pause();
      audioPlayer.currentTime = 0;
    }
  }

  async function play() {
    if (!item)
      return;

    console.log('play', item);

    if (audioPlayer) {
      stop();
      return;
    }

    const player = new Audio(`/audio/${item.audio}`);
    setAudioPlayer(player);

    player.addEventListener('complete', stop);
    player.addEventListener('ended', stop);
    player.addEventListener('durationchange', ({target}) => {
      setWaiting(false);
      setDuration((target as HTMLAudioElement).duration);
    });
    player.addEventListener('timeupdate', ({target}) => {
      setWaiting(false);
      setCurrentTime((target as HTMLAudioElement).currentTime);
    });
    player.addEventListener('waiting', () => setWaiting(true));

    await player.play();
  }

  async function shareItem() {
    try {
      await share(item);
    }
    catch (e) {
      setToast({ type: "error", text: `Share failed: ${e}`});
    }
  }

  useEffect(
    () => {
      let p = (((currentTime || 0) / (duration || 0) * 100) || 0) % 100;
      setProgressPercent(p);
    },
    [currentTime, duration]);

  return (
    <Card hoverable className={styles.audioPlayer}>
      <Card.Content className={styles.content} onClick={() => onClick && onClick()}>
        <Text h4>{item.title}</Text>
      </Card.Content>
      <Card.Footer className={styles.footer}>
        <a href={`/audio/${item.audio}`} download={item.audio}>
          <Button className={styles.btn} icon={<Download/>}
                  auto ghost size="small" aria-label="Download"/>
        </a>
        {canShare() && <Button className={styles.btn} icon={<Share2/>}
                               auto ghost size="small" aria-label="Share"
                               onClick={() => shareItem()}/>}
        <Button icon={progressPercent > 0 ? <Pause/> : <Play/>}
                auto size="small" loading={waiting} aria-label={waiting ? "Loading" : "Play"}
                onClick={() => play()}
                className={styles.progressBackground}
                style={{
                  background: `linear-gradient(90deg, var(--background-color) ${progressPercent}%, transparent 0%)`
                }}/>
      </Card.Footer>
    </Card>
  )
}
