import React from "react";
import {Page} from "@geist-ui/react";

export default function Layout({ children }) {
  return <Page>{children}</Page>;
}
