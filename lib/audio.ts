import YAML from "yaml";
import * as fs from "fs";
import {IAudioItem} from "./model";

// const file = fs.readFileSync("./audio.yml", "utf8");
// const data = YAML.parse(file);
//
// console.log(data);

// const data = [
//   {id: "first", title: "First"},
//   {id: "second", title: "Second"},
//   {id: "third", title: "Third"},
// ];

export async function getAudioDetails(): Promise<IAudioItem[]> {
  return new Promise<string>((resolve, reject) => {
    fs.readFile("lib/audio.yml", {encoding: "utf8"}, (err, data) => {
      if (err)
        return reject(err);
      resolve(data);
    });
  })
    .then(data => {
      return YAML.parse(data);
    })
    .then(data => data.audio);
}

export async function getAudioDetail(id): Promise<IAudioItem> {
  const audio = await getAudioDetails();
  return audio.find(a => a.id === id);
}
