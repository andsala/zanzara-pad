import Fuse from "fuse.js";
import {IAudioItem} from "./model";

export async function search(audio: any[], query: string): Promise<Fuse.FuseResult<IAudioItem>[]> {
  if (!query) {
    return audio.map(a => ({item: a, refIndex: 0}));
  }

  // TODO keep index stored
  const Fuse = (await import('fuse.js')).default
  const fuse = new Fuse(audio, {
    keys: ['title', 'text', 'characters', 'tags'],
    findAllMatches: true,
  });

  return fuse.search(query);
}
