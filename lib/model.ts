export interface IAudioItem {
  id: string;
  title: string;
  text: string;
  audio: string;
  characters: string[];
  tags: string[]
}
